# I Hackathon del Agua - Aguathon 2019 - Instituto Tecnológico de Aragón
SO: Ubuntu 18.04

Lenguaje de programación: Python3.6
## 1. Introducción
Este documento tiene como finalidad describir el modelo creado para la competición *Aguathon 2019* organizada por el *Instituto tecnológico de Aragón*. En la competición se propone predecir el nivel del Ebro a su paso por Zaragoza usando datos históricos. Para este propósito, hemos entrenado una red neuronal, la cual se explica en más detalle a continuación. Se puede acceder a todo el código a través el siguiente repositorio de GitLab: https://gitlab.com/zory/aguathon.
## 2. Uso
El primer paso es instalar las dependencias, esto se realiza a través del comando: *pip3 install -r requirements.txt*. El fichero _requirements.txt_ ya se encuetra incluido junto con el código fuente. 

Por último, una vez se han instalado las dependencias, hay que ejecutar la predicción usando *python3 predict.py*. Este comando tomará los datos presentes en la carpeta ENTRADA, un fichero llamado *datos.csv* y realizará las predicciones para los datos de entrada presentes en ese fichero. Una vez finalizado el proceso, generará un fichero csv en la carpeta SALIDA conteniendo todas las predicciones realizadas.
## 3. Desarrollo 
Hemos elegido Python3.6 como lenguaje de programación ya que cuenta con multitud de librerías para tratar los datos de una forma cómoda. Para hacer las predicciones, primero hemos de transformar los datos en un formato que pueda leer la red neuronal.
### 3.1 Leer CSV
Leemos el archivo CSV y los convertimos en un *DataFrame* de la librería *pandas*.
### 3.2 Descargar datos de AEMET
Descargamos los datos del tiempo en la ciudad de Zaragoza para las fechas necesarias. De aquí cogemos las precipitaciones y la temperatura media. Toda la información se puede encontrar en este enlace: https://opendata.aemet.es/centrodedescargas/inicio. En caso de que la descarga falle se selecciona otro modelo que no usa como entradas estos datos.
### 3.3 Redimensionar datos de entrada
Hemos decidido usar una red neuronal recurrente, con un *timestep* de 2, es decir, para cada predicción usará el registro actual más los de las dos horas anteriores. Después de probar varias configuraciones hemos comprobado que la mejor forma de alimentar a la red es darle los datos de uno en uno, de manera que el último dato que pasará será el nivel actual del Ebro en Zaragoza así la red aprende que datos debe almacenar y cuales olvidar. Al finalizar esta transformación, los datos están en una *Array* de la librería *numpy*.
### 3.4 Alimentar red
Por último, le pasamos los datos a la red y esta da una predicción, que almacenamos y escribimos al final de la ejecución en el CSV de salida.
## 4. Descripción de la red neuronal
En este apartado describimos la red neuronal. Para construirla se ha usado la librería *keras*, ya que permite construir y evaluar una red rápidamente. Se ha usado una red para cada predicción: 24 horas, 48 horas y 72 horas. Además, hemos construido otras tres redes sin usar los datos de AEMET, para en caso de que no se puedan descargar.
### 4.1 Celdas GRU
Hemos decidido usar neuronas de tipo GRU (Gated Recurrent Units). Estas son unas celdas relativamente nuevas usadas para construir redes neuronales recurrentes, una evaluación de las mismas se puede ver en: https://arxiv.org/pdf/1412.3555.pdf.
### 4.2 Número de capas y neuronas
Para determinar el número de capas y neuronas hemos usado fuerza bruta. Hemos probado todas las combinaciones con 2,3 y 4 capas y con 20, 30 y 40 neuronas en cada capa. El resultado ha sido el siguiente.

| Modelo | 24 horas | 48 horas | 72 horas |
| ----------- | ----------- | ----------- | ----------- |
| AEMET | (40,40,40) | (30,30,20,20) | (40,30,30,20) |
| Sin AEMET | (40,40) | (40,20,20,20) | (40,40,40,20) |

A estas capas hay que añadirle la capa de salida, una capa densa de una neurona que usa una función lineal como función de activación.
