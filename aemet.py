"""
This file is part of aguathon.

aguathon is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

aguathon is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with aguathon.  If not,
see <https://www.gnu.org/licenses/>.
"""
import http.client
import json
from pprint import pprint
from aemet_key import KEY  # To import key add file "aemet_key.py" with "KEY = ..."
import pandas as pd
import swifter
from datetime import date


def add_years(d, years):
    try:
        return d.replace(year=d.year + years)
    except ValueError:
        return d + (date(d.year + years, 1, 1) - date(d.year, 1, 1))


def get_aemet_data(date_col):
    conn = http.client.HTTPSConnection("opendata.aemet.es")

    headers = {'cache-control': "no-cache"}

    json_table = []

    estacion = "9434"  # Zaragoza, Aeropuerto
    # estacion = "9434P"  # Zaragoza, Valdespartera
    # estacion = "9001D"  # Reinosa, Cantabria (Nacimiento del ebro)

    fechaini = min(date_col)
    fechafin = max(date_col)

    while fechaini < fechafin:
        tries = 10
        while tries > 0:
            try:
                fechafin_aux = add_years(fechaini, 4)
                fechafin_aux = min(fechafin_aux, fechafin)
                conn.request("GET", "/opendata/api/valores/climatologicos/diarios/datos/fechaini/" +
                             fechaini.strftime('%Y-%m-%dT00:00:00UTC') + "/fechafin/" +
                             fechafin_aux.strftime('%Y-%m-%dT00:00:00UTC') + "/estacion/" + estacion +
                             "/?api_key=" + KEY, headers=headers)

                data = conn.getresponse().read()
                json_parsed = json.loads(data.decode("utf-8"))  # tmed: temperatura media
                url_data = json_parsed["datos"]

                conn.request("GET", url_data, headers=headers)
                data = conn.getresponse().read()
                json_parsed = json.loads(data.decode("utf-8"))
                json_table.extend(json_parsed)

                fechaini = fechafin_aux
                tries = -1
            except:
                tries -= 1
        if tries == 0:
            return None

    prec_table = date_col.swifter.apply(lambda date: extract_data_from_json(date, json_table))
    prec_table = prec_table.apply(pd.Series)
    prec_table.columns = ["prec", "tmed"]
    for col in prec_table:
        prec_table[col] = prec_table[col].str.replace(',', '.').replace('Ip', float('NaN')).astype(float)
        prec_table[col] = prec_table[col].ffill().fillna(0)

    return prec_table


def extract_data_from_json(date, json):
    for row in json:
        if "fecha" in row and row["fecha"] == date.strftime('%Y-%m-%d'):
            return row.get("prec", "Ip"), row.get("tmed", "Ip")
    return "Ip", "Ip"


if __name__ == "__main__":
    data = pd.read_csv("ENTRADA/datos.csv").ffill()
    date_col = pd.to_datetime(data["time"])[0:50]
    col = get_aemet_data(date_col)
    pprint(col)
