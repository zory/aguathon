"""
This file is part of aguathon.

aguathon is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

aguathon is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with aguathon.  If not,
see <https://www.gnu.org/licenses/>.
"""
import pandas as pd
from keras.engine.saving import model_from_json
from get_data import get_data, reshape_data
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


# Read data and download from AEMET
try:
    data = get_data("ENTRADA/datos.csv", aemet=True)
except:
    data = get_data("ENTRADA/datos.csv", aemet=False)

input_vars = ["year", "month", "hour", "RIESGO", "ALAGON_NR", "GRISEN_NR",
              "TAUSTE_NR", "NOVILLAS_NR", "TUDELA_NR", "ZGZ_NR"]
output_vars = ["24", "48", "72"]

# Detect model: with or without AEMET data
if "prec" in data and data["prec"][0] is not None:
    print("Success downloading data from AEMET, using full model...")
    input_vars.insert(0, "prec")
    input_vars.insert(0, "tmed")
else:
    print("Failure downloading data from AEMET, using partial model...")

# Reshape data to feed the model
print("Reshaping data...")
input_data = reshape_data(data[input_vars])
print("Done.")

out_frame = pd.DataFrame(data["time"])

for output_var in output_vars:
    print("Making predictions for " + output_var)

    if "prec" in data and data["prec"][0] is not None:
        model_name = "models/model_" + output_var + "_aemet.json"
        h5_name = "models/model_" + output_var + "_aemet.h5"
    else:
        model_name = "models/model_" + output_var + ".json"
        h5_name = "models/model_" + output_var + ".h5"

    # Load model
    json_file = open(model_name, 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    loaded_model.load_weights(h5_name)

    # Make prediction
    prediction = loaded_model.predict(input_data)
    out_frame["H" + output_var] = prediction
    print("Done predicting " + output_var)

print(out_frame)

# Write results in csv
print("Done. Writing in file...")
out_frame.to_csv("SALIDA/resultados.csv", index=False)
print("All done")
